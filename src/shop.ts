import { Coffee } from "./coffee";

export type CoffeeReader = () => Promise<Coffee>;
export type CoffeeValidator = (coffee: Coffee) => string;
export type CoffeePricer = (coffee: Coffee) => number;

export default async function coffeeShop(coffeeReader: CoffeeReader, validator: CoffeeValidator, pricer: CoffeePricer) {
    const coffee = await coffeeReader();
    const error = validator(coffee);
    return error || `Cost is $${pricer(coffee).toFixed(2)}`;
}
