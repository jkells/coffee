
export interface Coffee {
    hasMilk: boolean;
    hasSprinkles: boolean;
    isLarge: boolean;
    milkIsFoamed: boolean;
    numSugars: number;
}

const MAX_SUGARS = 9;
const EXPRESSO_PRICE = 1.50;
const LARGE_MILK_PRICE = 0.30;
const SMALL_MILK_PRICE = 0.25;
const SUGAR_UNIT_PRICE = 0.10;
const MILK_FOAMING_PRICE = 0.05;
const SERVICE_PRICE = 1.30;

export const INVALID_SUGAR_MESSAGE = `You can't have more than ${MAX_SUGARS} sugars, you will die.`;

export function coffeePrice(coffee: Coffee): number {
    const { hasMilk, isLarge, milkIsFoamed, numSugars} = coffee;
    const sugar = numSugars * SUGAR_UNIT_PRICE;
    const milk = hasMilk ? (isLarge ? LARGE_MILK_PRICE : SMALL_MILK_PRICE) : 0;
    const foam = milkIsFoamed ? MILK_FOAMING_PRICE : 0;
    const price = EXPRESSO_PRICE + SERVICE_PRICE + milk + sugar + foam;

    return Math.round(price * 100) / 100;
}

export function validateCoffee(coffee: Coffee): string {
    const { milkIsFoamed, hasMilk, numSugars } = coffee;
    const invalidFoam = !hasMilk && milkIsFoamed;
    const invalidSugars = numSugars > MAX_SUGARS;

    return invalidSugars ? INVALID_SUGAR_MESSAGE : "";
}
