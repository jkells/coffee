import { Coffee } from "./coffee";
import coffeeShop from "./shop";

describe("coffee shop", () => {
    const basicCoffee = {
        hasMilk: false,
        hasSprinkles: false,
        isLarge: false,
        milkIsFoamed: false,
        numSugars: 0,
    };

    it("should show errors if the coffee wasn't valid", async () => {
        const reader = () => new Promise<Coffee>((resolve) => resolve(basicCoffee));
        const validator = () => "error";
        const pricer = () => 10;
        expect(await coffeeShop(reader, validator, pricer)).toBe("error");
    });

    it("should output the price of a coffee with 2 decimal places", async () => {
        const reader = () => new Promise<Coffee>((resolve) => resolve(basicCoffee));
        const validator = () => "";
        const pricer = () => 10.4;
        expect(await coffeeShop(reader, validator, pricer)).toBe("Cost is $10.40");
    });
});
