import { coffeeReader, readBoolean, readNumber } from "./cli";

describe("read boolean", () => {
    it("should accept true, t or y", () => {
        expect(readBoolean("true")).toBe(true);
        expect(readBoolean("t")).toBe(true);
        expect(readBoolean("y")).toBe(true);
    });

    it("should accept false, f or n", () => {
        expect(readBoolean("false")).toBe(false);
        expect(readBoolean("f")).toBe(false);
        expect(readBoolean("n")).toBe(false);
    });

    it("should throw on invalid input", () => {
        expect(() => readBoolean("")).toThrowError();
        expect(() => readBoolean("not boolean")).toThrowError();
    });
});

describe("read number", () => {
    it("should accept positive numbers", () => {
        expect(readNumber("1")).toBe(1);
        expect(readNumber("2")).toBe(2);
        expect(readNumber("300")).toBe(300);
    });

    it("should throw on strings", () => {
        expect(() => readNumber("test")).toThrowError();
    });

    it("should throw on negative numbers", () => {
        expect(() => readNumber("-100")).toThrowError();
    });

    it("should throw on floats numbers", () => {
        expect(() => readNumber("-100.5")).toThrowError();
    });
});

describe("read coffee", () => {
    const createTestReader = (answers: string[]) => {
        let i = 0;
        return () => {
            if (i >= answers.length) {
                throw new Error("Asked too many questions");
            }
            return new Promise<string>((resolve) => resolve(answers[i++]));
        };
    };

    it("should ask 5 questions and output a coffee when handling a milk order", async () => {
        const reader = createTestReader([
            "TRUE",
            "T",
            "t",
            "5",
            "true",
        ]);

        const coffee = await (coffeeReader(reader))();
        expect(coffee).toEqual({
            hasMilk: true,
            hasSprinkles: true,
            isLarge: true,
            milkIsFoamed: true,
            numSugars: 5,
        });
    });

    it("should not ask about foaming milk or adding sprinkles, if you don't want milk!", async () => {
        const reader = createTestReader([
            "false",
            "false",
            "1",
        ]);

        const coffee = await (coffeeReader(reader))();
        expect(coffee).toEqual({
            hasMilk: false,
            hasSprinkles: false,
            isLarge: false,
            milkIsFoamed: false,
            numSugars: 1,
        });
    });
});
