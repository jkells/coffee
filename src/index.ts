
import * as readline from "readline";
import { coffeeReader } from "./cli";
import { coffeePrice, validateCoffee } from "./coffee";
import coffeeShop from "./shop";

function createReader(rl: readline.ReadLine) {
    return async (question: string) => {
        return new Promise<string>((resolve) => {
            rl.question(question, resolve);
        });
    };
}

async function main() {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
      });

    try {
        console.log(await coffeeShop(coffeeReader(createReader(rl)), validateCoffee, coffeePrice));
    } catch (e) {
        console.error(e.message);
    }

    rl.close();
}

main();
