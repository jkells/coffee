import {
    coffeePrice,
    INVALID_SUGAR_MESSAGE,
    validateCoffee,
} from "./coffee";

describe("Coffee pricing", () => {
    it("Should charge $2.80 for a plain espresso", () => {
        expect(coffeePrice({
            hasMilk: false,
            hasSprinkles: false,
            isLarge: false,
            milkIsFoamed: false,
            numSugars: 0,
        })).toBe(2.80);
    });

    it("Should charge $3.05 for a plain small milk coffee", () => {
        expect(coffeePrice({
            hasMilk: true,
            hasSprinkles: false,
            isLarge: false,
            milkIsFoamed: false,
            numSugars: 0,
        })).toBe(3.05);
    });

    it("Should charge $3.10 for a plain large milk coffee", () => {
        expect(coffeePrice({
            hasMilk: true,
            hasSprinkles: false,
            isLarge: true,
            milkIsFoamed: false,
            numSugars: 0,
        })).toBe(3.10);
    });

    it("Should charge $3.15 for a foamed large milk coffee", () => {
        expect(coffeePrice({
            hasMilk: true,
            hasSprinkles: false,
            isLarge: true,
            milkIsFoamed: true,
            numSugars: 0,
        })).toBe(3.15);
    });

    it("Should charge $3.35 for a foamed large milk coffee with 2 sugars", () => {
        expect(coffeePrice({
            hasMilk: true,
            hasSprinkles: false,
            isLarge: true,
            milkIsFoamed: true,
            numSugars: 2,
        })).toBe(3.35);
    });

    it("Should charge $3.35 for a foamed large milk coffee with 2 sugars and sprinkles", () => {
        expect(coffeePrice({
            hasMilk: true,
            hasSprinkles: true,
            isLarge: true,
            milkIsFoamed: true,
            numSugars: 2,
        })).toBe(3.35);
    });
});

describe("Coffee validation", () => {
    it("Should reject more than 9 sugars", () => {
        expect(validateCoffee({
            hasMilk: false,
            hasSprinkles: false,
            isLarge: false,
            milkIsFoamed: false,
            numSugars: 10,
        })).toBe(INVALID_SUGAR_MESSAGE);
    });

    it("should return an empty string for a valid coffee", () => {
        expect(validateCoffee({
            hasMilk: false,
            hasSprinkles: false,
            isLarge: false,
            milkIsFoamed: false,
            numSugars: 0,
        })).toBe("");
    });
});
