export type Reader = (question: string) => Promise<string>;

export function readBoolean(value: string) {
    const lower = value.toLowerCase();
    const truethy = lower === "true" || lower === "t" || lower === "y";
    const falsey = lower === "false" || lower === "f" || lower === "n";

    if (!truethy && !falsey) {
        throw new Error("Please specify true or false");
    }

    return truethy;
}

export function readNumber(answer: string): number {
    const value = Number(answer);
    if (!Number.isInteger(value) || value < 0) {
        throw new Error("Please specify a positive integer");
    }
    return value;
}

export function coffeeReader(reader: Reader) {
    return async () => {
        const isLarge = readBoolean(await reader("Large? (true/false) "));
        const hasMilk = readBoolean(await reader("Milk? (true/false) "));
        const milkIsFoamed = hasMilk ? readBoolean(await reader("Foam the milk? (true/false) ")) : false;
        const numSugars = readNumber(await reader("How many sugars? "));
        const hasSprinkles = hasMilk ? readBoolean(await reader("Add chocolate sprinkles? (true/false) ")) : false;

        return {
            hasMilk,
            hasSprinkles,
            isLarge,
            milkIsFoamed,
            numSugars,
        };
    };
}
